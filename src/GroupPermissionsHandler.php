<?php

declare(strict_types = 1);

namespace Drupal\group_permissions_parameter;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupTypeInterface;
use Drupal\group_permissions\Entity\GroupPermission;
use Drupal\group_permissions\GroupPermissionsManager;

/**
 * Service class for settings a group permissions on the group level.
 *
 * This service can be used by any third party code to set the permissions of a
 * group on the group level.
 */
class GroupPermissionsHandler implements GroupPermissionsHandlerInterface {

  use StringTranslationTrait;

  /**
   * The group permissions manager service.
   *
   * @var \Drupal\group_permissions\GroupPermissionsManager
   */
  protected $groupPermissionsManager;

  /**
   * Current active user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $dateTime;

  /**
   * GroupPermissionsHandler constructor.
   *
   * @param \Drupal\group_permissions\GroupPermissionsManager $group_permissions_manager
   *   The group permissions manager service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   Current active user.
   * @param \Drupal\Component\Datetime\TimeInterface $date_time
   *   Drupal time service.
   */
  public function __construct(GroupPermissionsManager $group_permissions_manager, AccountProxyInterface $current_user, TimeInterface $date_time) {
    $this->groupPermissionsManager = $group_permissions_manager;
    $this->currentUser = $current_user;
    $this->dateTime = $date_time;
  }

  /**
   * {@inheritdoc}
   */
  public function setGroupPermissions(GroupInterface $group, array $mutable_permissions, bool $sync = FALSE): int {
    if (empty($mutable_permissions)) {
      return 0;
    }

    $group_permission = $this->getGroupPermissionObject($group);

    if (!$group_permission) {
      return 0;
    }

    // When syncing the permissions, get the default permissions by the group
    // type. Otherwise, get permissions stored in the group permission entity.
    $group_permissions = $sync
      ? $this->addMutablePermissions($this->getDefaultPermissionsByGroupType($group->getGroupType()), $mutable_permissions)
      : $this->addMutablePermissions($group_permission->getPermissions(), $mutable_permissions);

    // Set the new permissions and add a revision message.
    $group_permission->setPermissions($group_permissions);
    $group_permission->setRevisionUserId($this->currentUser->id());
    $group_permission->setRevisionCreationTime($this->dateTime->getRequestTime());
    $group_permission->setRevisionLogMessage($sync
      ? $this->t('Sync group permissions with permissions from the group type.')
      : $this->t('Set group permissions on group save.'));

    $violations = $group_permission->validate();
    if (count($violations) > 0) {
      $message = '';
      foreach ($violations as $violation) {
        $message .= "\n" . $violation->getMessage();
      }
      throw new EntityStorageException('Group permissions are not saved correctly, because:' . $message);
    }
    return $group_permission->save();
  }

  /**
   * Add the mutable permissions.
   *
   * @param array $group_permissions
   *   Either the permissions from the group type level or the permissions from
   *   the group permission entity.
   * @param array $mutable_permissions
   *   The mutable permissions to apply to the group permission.
   *
   * @return array
   *   The final permissions settings.
   */
  protected function addMutablePermissions(array $group_permissions, array $mutable_permissions): array {
    foreach ($mutable_permissions as $role => $permissions) {
      foreach ($permissions as $permission => $status) {
        if ($status) {
          // Add the permission to the role if not already exists.
          if (!array_key_exists($role, $group_permissions) || !in_array($permission, $group_permissions[$role], TRUE)) {
            $group_permissions[$role][] = $permission;
          }
        }
        else {
          // Remove the permission from the role if already exists.
          if (array_key_exists($role, $group_permissions) || in_array($permission, $group_permissions[$role], TRUE)) {
            $group_permissions[$role] = array_diff($group_permissions[$role], [$permission]);
          }
        }
      }
    }

    return $group_permissions;
  }

  /**
   * Get the default permissions for the given group type.
   *
   * @param \Drupal\group\Entity\GroupTypeInterface $groupType
   *   The group type to return the permissions for.
   *
   * @return array
   *   An array of permissions keyed by role.
   */
  protected function getDefaultPermissionsByGroupType(GroupTypeInterface $groupType): array {
    $permissions = [];

    foreach ($groupType->getRoles() as $group_role_id => $group_role) {
      $permissions[$group_role_id] = $group_role->getPermissions();
    }

    return $permissions;
  }

  /**
   * Get the groupPermission object, will create a new one if needed.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group to get the group permission object for.
   *
   * @return \Drupal\group_permissions\Entity\GroupPermission|null
   *   The (new) group permission object, returns NULL if something went wrong.
   */
  protected function getGroupPermissionObject(GroupInterface $group): ?GroupPermission {
    /** @var \Drupal\group_permissions\Entity\GroupPermission $groupPermission */
    $group_permission = $this->groupPermissionsManager->getGroupPermission($group);

    if ($group_permission === NULL) {
      // Create the entity.
      $group_permission = GroupPermission::create([
        'gid' => $group->id(),
        'permissions' => $this->getDefaultPermissionsByGroupType($group->getGroupType()),
        'status' => 1,
      ]);
    }
    else {
      $group_permission->setNewRevision(TRUE);
    }

    return $group_permission;
  }

}
