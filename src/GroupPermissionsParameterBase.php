<?php

declare(strict_types = 1);

namespace Drupal\group_permissions_parameter;

use Drupal\Core\Plugin\PluginBase;
use Drupal\group\Entity\GroupInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for group permissions parameter plugin instances.
 */
abstract class GroupPermissionsParameterBase extends PluginBase implements GroupPermissionsParameterInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getBundles(): array {
    return $this->getPluginDefinition()['bundles'];
  }

  /**
   * {@inheritdoc}
   */
  public function isApplicable(GroupInterface $group, string $operation): bool {
    if (!in_array($operation, ['create', 'update', 'sync'])) {
      throw new \InvalidArgumentException("The operation should be 'create', 'update' or 'sync'. $operation is passed.");
    }

    switch ($operation) {
      case 'create':
        $is_applicable = $this->isAppliedOnCreate($group);
        break;

      case 'update':
        $is_applicable = $this->isAppliedOnUpdate($group);
        break;

      case 'sync':
        $is_applicable = $this->isAppliedOnSync($group);
        break;

      default:
        $is_applicable = FALSE;
    }

    return $is_applicable;
  }

}
