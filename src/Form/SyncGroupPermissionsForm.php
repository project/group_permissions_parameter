<?php

declare(strict_types = 1);

namespace Drupal\group_permissions_parameter\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupType;
use Drupal\group_permissions_parameter\GroupPermissionsParameterManagerInterface;
use Drupal\group_permissions_parameter\GroupPermissionsSyncBatch;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sync group permissions form.
 */
class SyncGroupPermissionsForm extends FormBase {

  /**
   * The group permissions parameter sync batch service.
   *
   * @var \Drupal\group_permissions_parameter\GroupPermissionsSyncBatch
   */
  protected $groupPermissionsParameterSyncBatch;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Group permissions parameter manager service.
   *
   * @var \Drupal\group_permissions_parameter\GroupPermissionsParameterManagerInterface
   */
  public $groupPermissionsParameterManager;

  /**
   * Constructs a new SyncGroupPermissionsForm object.
   *
   * @param \Drupal\group_permissions_parameter\GroupPermissionsSyncBatch $group_permissions_parameter_sync_batch
   *   The group permissions parameter sync batch service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\group_permissions_parameter\GroupPermissionsParameterManagerInterface $group_permissions_parameter_manager
   *   The group permissions parameter manager service.
   */
  public function __construct(GroupPermissionsSyncBatch $group_permissions_parameter_sync_batch, EntityTypeManagerInterface $entity_type_manager, GroupPermissionsParameterManagerInterface $group_permissions_parameter_manager) {
    $this->groupPermissionsParameterSyncBatch = $group_permissions_parameter_sync_batch;
    $this->entityTypeManager = $entity_type_manager;
    $this->groupPermissionsParameterManager = $group_permissions_parameter_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('group_permissions_parameter.sync_batch'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.group_permissions_parameter'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'group_permissions_parameter_sync_permissions_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['group_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Group types'),
      '#description' => $this->t('Select the group types that you would like to sync the group permissions for.'),
      '#options' => $this->getGroupTypes(),
      '#required' => TRUE,
      '#multiple' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Sync permissions'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->groupPermissionsParameterSyncBatch->build($form_state->getValue('group_types'));
  }

  /**
   * Get group types that has a group permission parameter.
   *
   * @return array
   *   The group types.
   */
  protected function getGroupTypes(): array {
    $bundle_options = [];
    $group_types = GroupType::loadMultiple();
    foreach ($group_types as $group_type) {
      // Skip if the group type doesn't have group permission parameter
      // plugins.
      if (!$this->groupPermissionsParameterManager->getDefinitionsByGroupType($group_type)) {
        continue;
      }

      // Get the count of the groups by the bundle.
      $group_storage = $this->entityTypeManager->getStorage('group');
      $group_ids = $group_storage->getQuery()
        ->accessCheck(FALSE)
        ->condition('type', $group_type->id())
        ->execute();
      $bundle_options[$group_type->id()] = $group_type->label() . ' (' . count($group_ids) . ')';
    }

    return $bundle_options;
  }

}
