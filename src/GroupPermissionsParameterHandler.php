<?php

declare(strict_types = 1);

namespace Drupal\group_permissions_parameter;

use Drupal\group\Entity\GroupInterface;

/**
 * Service class for handling a group permissions parameter.
 */
class GroupPermissionsParameterHandler implements GroupPermissionsParameterHandlerInterface {

  /**
   * The group permissions parameter manager service.
   *
   * @var \Drupal\group_permissions_parameter\GroupPermissionsParameterManager
   */
  protected $groupPermissionsParameterManager;

  /**
   * The group permissions handler service.
   *
   * @var \Drupal\group_permissions_parameter\GroupPermissionsHandlerInterface
   */
  protected $groupPermissionsHandler;

  /**
   * GroupPermissionsParameterHandler constructor.
   *
   * @param \Drupal\group_permissions_parameter\GroupPermissionsParameterManager $group_permissions_parameter_manager
   *   The group permissions parameter plugin manager.
   * @param \Drupal\group_permissions_parameter\GroupPermissionsHandlerInterface $group_permissions_handler
   *   The group permissions handler service.
   */
  public function __construct(GroupPermissionsParameterManager $group_permissions_parameter_manager, GroupPermissionsHandlerInterface $group_permissions_handler) {
    $this->groupPermissionsParameterManager = $group_permissions_parameter_manager;
    $this->groupPermissionsHandler = $group_permissions_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function setGroupPermissions(GroupInterface $group, string $operation): int {
    // @todo Add early return (that return when the group type doesn't support
    //   group permissions) when
    //   https://www.drupal.org/project/group_permissions/issues/3276738 is
    //   fixed.
    try {
      $permissions = $this->groupPermissionsParameterManager->getApplicablePermissions($group, $operation);
      return $this->groupPermissionsHandler->setGroupPermissions($group, $permissions, $operation === 'sync');
    }
    catch (\Exception $e) {
      watchdog_exception('group_permissions_parameter', $e);
    }

    return 0;
  }

}
