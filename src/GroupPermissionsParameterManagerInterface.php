<?php

declare(strict_types = 1);

namespace Drupal\group_permissions_parameter;

use Drupal\group\Entity\GroupInterface;

/**
 * Interface for plugin type manager for GroupPermissionsParameter plugins.
 */
interface GroupPermissionsParameterManagerInterface {

  /**
   * Get permissions by the applicable group permissions parameters.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group.
   * @param string $operation
   *   The operation; either create, update or sync.
   *
   * @return array
   *   The applicable permissions.
   */
  public function getApplicablePermissions(GroupInterface $group, string $operation): array;

}
