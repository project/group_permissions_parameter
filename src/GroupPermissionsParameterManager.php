<?php

declare(strict_types = 1);

namespace Drupal\group_permissions_parameter;

use Drupal\Component\Utility\SortArray;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupTypeInterface;
use Drupal\group_permissions_parameter\Annotation\GroupPermissionsParameter;

/**
 * Plugin type manager for GroupPermissionsParameter plugins.
 */
class GroupPermissionsParameterManager extends DefaultPluginManager implements GroupPermissionsParameterManagerInterface {

  /**
   * Constructs a new GroupPermissionsPluginManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/GroupPermissionsParameter', $namespaces, $module_handler, GroupPermissionsParameterInterface::class, GroupPermissionsParameter::class);
    $this->alterInfo('group_permissions_parameter_info');
    $this->setCacheBackend($cache_backend, 'group_permissions_parameter_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function getApplicablePermissions(GroupInterface $group, string $operation): array {
    $plugins = $this->getDefinitionsByGroupType($group->getGroupType());

    $permissions = [];
    foreach ($plugins as $plugin_id => $definition) {
      /** @var \Drupal\group_permissions_parameter\GroupPermissionsParameterInterface $pluginInstance */
      $pluginInstance = $this->createInstance($plugin_id);
      if ($pluginInstance && $pluginInstance->isApplicable($group, $operation)) {
        $permissions = array_merge($permissions, $pluginInstance->getPermissions($group));
      }
    }

    return $permissions;
  }

  /**
   * Get group permissions definitions by group type.
   *
   * @param \Drupal\group\Entity\GroupTypeInterface $group_type
   *   The type of the group.
   *
   * @return array
   *   The group permissions definitions.
   */
  public function getDefinitionsByGroupType(GroupTypeInterface $group_type): array {
    $definitions = [];
    foreach (parent::getDefinitions() as $plugin_id => $definition) {
      if (in_array($group_type->id(), $definition['bundles'])) {
        $definitions[$plugin_id] = $definition;
      }
    }
    uasort($definitions, [SortArray::class, 'sortByWeightElement']);
    return $definitions;
  }

}
