<?php

declare(strict_types = 1);

namespace Drupal\group_permissions_parameter;

use Drupal\group\Entity\GroupInterface;

/**
 * Interface for Group permissions handler service.
 */
interface GroupPermissionsHandlerInterface {

  /**
   * Set permissions for a given group.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group entity.
   * @param array $mutable_permissions
   *   The permissions per role to apply to the group permission.
   * @param bool $sync
   *   Whether to sync the permissions of the group with the group type level
   *   permissions.
   *
   * @return int
   *   Returns 0 if permissions haven't been saved, otherwise return either
   *   SAVED_NEW or SAVED_UPDATED, depending on the operation performed.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setGroupPermissions(GroupInterface $group, array $mutable_permissions, bool $sync = FALSE): int;

}
