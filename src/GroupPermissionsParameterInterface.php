<?php

declare(strict_types = 1);

namespace Drupal\group_permissions_parameter;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\group\Entity\GroupInterface;

/**
 * Interface for group permissions parameter plugin instances.
 */
interface GroupPermissionsParameterInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * Get bundles.
   *
   * @return array
   *   The bundles.
   */
  public function getBundles(): array;

  /**
   * Get the permissions.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group entity.
   *
   * @return array
   *   The permissions for the parameter(s).
   */
  public function getPermissions(GroupInterface $group): array;

  /**
   * Check if the group permissions parameter is applied on group create.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group entity.
   *
   * @return bool
   *   True if the plugin is applicable on create, False otherwise.
   */
  public function isAppliedOnCreate(GroupInterface $group): bool;

  /**
   * Check if the group permissions parameter is applied on group update.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group entity.
   *
   * @return bool
   *   True if the plugin is applicable on update, False otherwise.
   */
  public function isAppliedOnUpdate(GroupInterface $group): bool;

  /**
   * Check if the group permissions parameter is applied on sync permissions.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group entity.
   *
   * @return bool
   *   True if the plugin is applicable on sync, False otherwise.
   */
  public function isAppliedOnSync(GroupInterface $group): bool;

  /**
   * Check if the group permissions parameter is applicable.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group entity.
   * @param string $operation
   *   The operation; either create, update or sync.
   *
   * @return bool
   *   True if the plugin is applicable, False otherwise.
   */
  public function isApplicable(GroupInterface $group, string $operation): bool;

}
