<?php

declare(strict_types = 1);

namespace Drupal\group_permissions_parameter\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an group_permissions_parameter annotation object.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class GroupPermissionsParameter extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the group permissions parameter plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A brief description of the group permissions parameter.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description = '';

  /**
   * The bundles.
   *
   * @var array
   */
  public $bundles = [];

  /**
   * The plugin weight.
   *
   * The weight of the plugin, the higher the number, the later the plugin
   * is being called.
   *
   * @var int
   */
  public $weight = 0;

}
