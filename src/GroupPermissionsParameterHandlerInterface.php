<?php

declare(strict_types = 1);

namespace Drupal\group_permissions_parameter;

use Drupal\group\Entity\GroupInterface;

/**
 * Interface for Group permissions parameter handler service.
 */
interface GroupPermissionsParameterHandlerInterface {

  /**
   * Set permissions for a given group.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group entity.
   * @param string $operation
   *   The operation; either create, update or sync.
   *
   * @return int
   *   Returns 0 if permissions haven't been saved, otherwise return either
   *   SAVED_NEW or SAVED_UPDATED, depending on the operation performed.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setGroupPermissions(GroupInterface $group, string $operation): int;

}
