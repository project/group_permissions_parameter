<?php

declare(strict_types = 1);

namespace Drupal\group_permissions_parameter;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Batch service for group permissions syncing.
 */
class GroupPermissionsSyncBatch {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * Group permissions parameter handler.
   *
   * @var \Drupal\group_permissions_parameter\groupPermissionsParameterHandlerInterface
   */
  private $groupPermissionsParameterHandler;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * GroupPermissionsSyncBatch constructor.
   *
   * @param \Drupal\group_permissions_parameter\groupPermissionsParameterHandlerInterface $group_permissions_parameter_handler
   *   The group permissions parameter handler service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The core messenger service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   */
  public function __construct(
    groupPermissionsParameterHandlerInterface $group_permissions_parameter_handler,
    EntityTypeManagerInterface $entity_type_manager,
    MessengerInterface $messenger,
    LoggerChannelFactoryInterface $logger_factory) {
    $this->groupPermissionsParameterHandler = $group_permissions_parameter_handler;
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->logger = $logger_factory->get('group permissions parameter');
  }

  /**
   * Build an syncing group permissions batch for a given group types.
   *
   * @param array $bundles
   *   The group types.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function build(array $bundles): void {
    // Retrieve all groups of this type.
    $group_storage = $this->entityTypeManager->getStorage('group');
    $group_ids = $group_storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', $bundles, 'IN')
      ->execute();

    // @todo consider using alterable calls list strategy.
    // Create the operations for the batch.
    $batch_id = 1;
    if (!empty($group_ids)) {
      $batch_builder = (new BatchBuilder())
        ->setTitle($this->t('Syncing group permissions'))
        ->setInitMessage($this->t('Start group permissions syncing.'))
        ->setProgressMessage($this->t('Completed @current operation of @total.'))
        ->setErrorMessage($this->t('Syncing group permissions has encountered an error.'))
        ->setFinishCallback([$this, 'finish']);

      // Add operation per chunk of groups.
      foreach (array_chunk($group_ids, 25) as $gids) {
        // Prepare the operation.
        $batch_builder->addOperation([$this, 'process'], [
          $batch_id,
          $gids,
          count($group_ids),
        ]);

        $batch_id++;
      }

      batch_set($batch_builder->toArray());
    }
    else {
      $this->messenger->addWarning($this->t('No groups found by the selected group types.'));
    }
  }

  /**
   * Batch process callback.
   *
   * Used to sync group permissions for chuck of groups.
   *
   * @param int $id
   *   Id of the batch.
   * @param array $gids
   *   Chuck of group ids.
   * @param int $groups_count
   *   The total count of all groups.
   * @param array $context
   *   The context of the batch.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function process(int $id, array $gids, int $groups_count, array &$context): void {
    if (!isset($context['results']['total'])) {
      $context['results']['total'] = $groups_count;
      $context['results']['synced_count'] = 0;
      $context['results']['not_synced_count'] = 0;
    }

    // Sync the permissions for the passed groups.
    foreach ($gids as $gid) {
      /** @var \Drupal\group\Entity\GroupInterface $group */
      $group = $this->entityTypeManager->getStorage('group')->load($gid);

      if ($this->groupPermissionsParameterHandler->setGroupPermissions($group, 'sync')) {
        $context['results']['synced_count'] = ++$context['results']['synced_count'];
      }
      else {
        $this->logger->error('Failed to sync the group permissions for group @gid of type @group_type', [
          '@gid' => $gid,
          '@group_type' => $group->bundle(),
        ]);
        $context['results']['not_synced_count'] = ++$context['results']['not_synced_count'];
      }
    }

    // Display message on top of the progressbar.
    $context['message'] = $this->t('Batch @id: Completed syncing the group permissions for @count of @total groups.', [
      '@id' => $id,
      '@count' => $context['results']['synced_count'],
      '@total' => $context['results']['total'],
    ]);
  }

  /**
   * Batch Finished callback.
   *
   * @param bool $success
   *   Success of the operation.
   * @param array $results
   *   Array of results for post processing.
   * @param array $operations
   *   Array of operations.
   */
  public function finish($success, array $results, array $operations): void {
    if ($success) {
      if (!empty($results['synced_count'])) {
        $this->messenger->addMessage($this->t('Successfully synced the group permissions for @synced_count of @total groups.', [
          '@total' => $results['total'],
          '@synced_count' => $results['synced_count'],
        ]));

        // Add warning when couldn't sync the permissions for all groups.
        if ($results['synced_count'] !== $results['total']) {
          $this->messenger->addWarning($this->t('Could not sync the group permissions for all groups of the selected group types. See the log for more information.'));
        }
      }
      else {
        $this->messenger->addWarning($this->t('Could not sync the group permissions for groups of the selected group types. See the log for more information.'));
      }
    }
    else {
      // An error occurred.
      $error_operation = reset($operations);
      $this->messenger->addError($this->t('An error occurred while processing @operation with arguments : @args', [
        '@operation' => $error_operation[0],
        '@args' => print_r($error_operation[0], TRUE),
      ]));
    }
  }

}
