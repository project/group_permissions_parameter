CONTENTS OF THIS FILE
---------------------

* Introduction
* Features
* Example

INTRODUCTION
------------

This module provides API to set group permissions on the group level using
[Group permissions](https://www.drupal.org/project/group_permissions)
permissions module based on custom parameters (e.g. field in the group). Setting
the permissions of a group is executed when saving it.


FEATURES
------------

 - Provides plugin base to set your mutable permissions (the permissions that
   depend on your parameters) while using the the permissions UI that comes with
   the group module to set the common permissions (the permissions that don't
   depend on parameters - fixed permissions).
 - Provides a service for setting your mutable permissions using other way than
   creating GroupPermissionsParameter plugin instances. You identify the way
   yourself.
 - To use GroupPermissionsParameter plugin for setting your mutable permissions,
   you could create a plugin instance per parameter or one plugin instance for
   multiple parameters
 - Provides a form to sync the permissions on the group level with permissions
   from the group type level. This is useful in case new permissions should be
   granted to/revoked from existing groups. In your plugin, you can identify
   whether the mutable permissions should be applied on syncing or not. The form
   can be found under Groups menu.


EXAMPLE
------------

```
<?php

declare(strict_types = 1);

namespace Drupal\custom_module\Plugin\GroupPermissionsParameter;

use Drupal\group\Entity\GroupInterface;
use Drupal\group_permissions_parameter\GroupPermissionsParameterBase;

/**
 * Plugin for group privacy group permissions parameter.
 *
 * @GroupPermissionsParameter(
 *   id = "my_group_privacy",
 *   label = @Translation("My group privacy"),
 *   description = @Translation("Custom permissions for my group based on the group privacy"),
 *   bundles = {
 *     "my_group",
 *   },
 *   weight = 0,
 * )
 */
class myGroupPrivacy extends GroupPermissionsParameterBase {

  /**
   * {@inheritdoc}
   */
  public function getPermissions(GroupInterface $group): array {
    $permissions = [];

    // Setup permissions for anonymous.
    $permissions['my_group-anonymous'] = [
      'view group_node:page entity' => $group->isPublic(),
    ];

    // Setup permissions for outsider.
    $permissions['my_group-outsider'] = [
      'join group' => $group->isPublic(),
      'view group_node:page entity' => $group->isPublic(),
    ];

    return $permissions;
  }

  /**
   * {@inheritdoc}
   */
  public function isAppliedOnCreate(GroupInterface $group): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function isAppliedOnUpdate(GroupInterface $group): bool {
    if (!empty($group) && !empty($group->original)) {
      if ($group->getPrivacy() !== $group->original->getPrivacy()) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isAppliedOnSync(GroupInterface $group): bool {
    return TRUE;
  }

}
```